<?php

/**
 * 
 * Copyrights         : WG
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 */

jimport('joomla.application.component.controller');

class wgyandexfotkiController extends JController
{

        function display($cachable = false, $urlparams = false)
        {

                // Retrieve the current view
                $document = JFactory::getDocument();
                $viewName  = JRequest::getVar( 'view', 'display' );
                $viewType  = $document->getType();
		  //print '<pre>'; print $viewName; print $viewType; print '</pre>';
                $view = $this->getView($viewName, $viewType);

                // Set the correct model for the view
                $model  = $this->getModel('wgyandexfotki');
		  //print '<pre>'; var_dump($model); print '</pre>';	
        	  $view->setModel( $model, true );

                // Display the view
                parent::display();

        }

}

?>

