<?php

/**
 * 
 * Copyrights         : WG
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

jimport( 'joomla.application.component.model' );
//wgyandexfotkiModelWgyandexfotki
class wgyandexfotkiModelWgyandexfotki extends JModel {
        
        function loadWGhelpers() {
                
                ini_set("include_path", ini_get("include_path") . PATH_SEPARATOR . JPATH_COMPONENT . DS . 'libraries');
                require_once('wgjoomla' . DS . 'wgloader.php');
        }

        function getParam($name) {
                $mainframe = JFactory::getApplication();

                if (!wgCache::hasVar('params')) {
                        //wgLog::logLine("fill cache(params)");
                        $params = $mainframe->getParams();
                        wgCache::saveVar('params',$params);        
                }
                $params = wgCache::loadVar('params');
                return $params->get($name);
        }

        function getParams() {
                $mainframe = JFactory::getApplication();
                
                $this->loadWGhelpers();
                if (!wgCache::hasVar('params')) {
                        //wgLog::logLine("fill cache(params)");
                        $params = $mainframe->getParams();
                        wgCache::saveVar('params',$params);        
                }
                return wgCache::loadVar('params');
        }

        function getyandexfotkiUsername() {
                return $this->getParam('yandexfotkiUsername');
        }

        function getMaxPhotosPerPage() {
                return $this->getParam('yandexfotkiMaxPhotosPerPage');
        }
        
        function initWgGdata($reset=false) {
                
                $this->loadWGhelpers();
                // init (always!) with defaults
                if ($reset) wgCache::clearVar('params');
                $thumb = $this->getParam('yandexfotkiThumbnailImagesize');
                if (empty($thumb)) $thumb = 144;
                $imgsiz = $this->getParam('yandexfotkiImagesize');
                if (empty($imgsiz)) $imgsiz = 640;
                wgGdata::inityandexfotki($this->getyandexfotkiUsername(),'','', $thumb, $imgsiz);
        }

        function getAlbumsList() {
                
                // 2011/8/28/patch
                $this->loadWGhelpers();
                $this->initWgGdata(true);
                $albumsList = wgGdata::getyandexfotkiAlbums();
                wgCache::saveVar('singleAbumView', '0');
                return $albumsList;
        }

        function getAlbum() {

                // 2011/8/28/patch
                $this->loadWGhelpers();
                $this->initWgGdata(true);
                $maxPhotosPerPage = $this->getMaxPhotosPerPage();
                $album = JRequest::getVar('album');
                if ($album=='') {
                        // in config param -> singleView 
                        $album = $this->getParam('yandexfotkiAlbumid');
                        if ($album=='') {
                                $albumsList = $this->getAlbumsList();
                                if (is_array($albumsList) && count($albumsList)>0) {
                                        // then first album found
                                        $album = $albumsList[0]['albumID'];
                                }
                        }
                        JRequest::setVar('album', $album);
                        // mark single view
                        wgCache::saveVar('singleAbumView', '1');
                }
                $page = JRequest::getVar('page');
                if (!$page) { 
                        $page = 1;
                        JRequest::setVar('page', $page);
                }
                $album = wgGdata::getyandexfotkiAlbumPhotos($album,$maxPhotosPerPage * ($page-1),$maxPhotosPerPage);
                return $album;
        }
        
        function getSingleAlbumView() {
                
                $this->loadWGhelpers();
                $singleAbumView = wgCache::loadVar('singleAbumView');
                //wgLog::logLine("singleAbumView=$singleAbumView");
                return $singleAbumView;
        }


}
