<?php

/**
 * 
 * Copyrights         : WG
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

$mainframe = JFactory::getApplication();
$maindoc = JFactory::getDocument();

$jquery = true;
if ($jquery) {

        //$script = "<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js'></script>";
        $script = "<script type='text/javascript' src='http://code.jquery.com/jquery-latest.min.js'></script>";
        $maindoc->addCustomTag($script);
        $maindoc->addCustomTag( '<script type="text/javascript">jQuery.noConflict();</script>' );
        $slimboxScript = "<script type='text/javascript' src='" . $this->baseurl . "/components/com_wgyandexfotki/libraries/slimbox/slimbox2.js'></script>";
        $maindoc->addCustomTag($slimboxScript);
        $slimboxCss = "<link rel='stylesheet' href='" . $this->baseurl . "/components/com_wgyandexfotki/libraries/slimbox/slimbox2.css' type='text/css' media='screen' />";
        $maindoc->addCustomTag($slimboxCss);
        
} else {
        $script = "<script type='text/javascript' src='" . $this->baseurl . "/media/system/js/mootools.js'></script>";
        $slimboxScript = "<script type='text/javascript' src='" . $this->baseurl . "/components/com_wgyandexfotki/libraries/slimbox/slimbox.js'></script>";
        $maindoc->addCustomTag($script);
        $maindoc->addCustomTag($slimboxScript);
        $slimboxCss = "<link rel='stylesheet' href='" . $this->baseurl . "/components/com_wgyandexfotki/libraries/slimbox/slimbox.css' type='text/css' media='screen' />";
        $maindoc->addCustomTag($slimboxCss);
}

$title = $this->params->get('yandexfotkiWelcomeTitle');
if ($title) echo "<h1>$title</h1>";

$text = $this->params->get('yandexfotkiWelcomeText');
if ($text) echo "<p>$text</p>";

echo "<strong>" . sizeof($this->albumsList) . "</strong>" . " " . JText::_('WGP_albums_present') . " " . "<strong>" . $this->params->get('yandexfotkiUsername') . "</strong><br /><br />";

echo "<p align=center>";

echo "<div class=\"wgPicasa\"><table>";
for ($i = 0; $i < sizeof($this->albumsList); $i++){
  $album = $this->albumsList[$i];
  if ($i % $this->params->get('yandexfotkiMaxPhotosPerRow') == 0)
    echo "<tr valign=\"middle\">";
        echo "<td align=\"center\">";
        $link = JRoute::_('index.php?option=com_wgyandexfotki&view=album&album=' .  $album['albumID'] . '&page=1');
        echo "<a href=\"" . $link . "\">";
        echo "<img ";
        //echo "class=instant ";
        //echo "src='".$album['thumbURL']."' width='".$this->params->get('yandexfotkiThumbnailImagesize')."' /><br />";
        echo "src=\"".$album['thumbURL']."\" /><br />";
        echo $album['albumTitle'];
        echo "</a>";
    echo "</td>";
        if ( ($i + $this->params->get('yandexfotkiMaxPhotosPerRow') + 1) % $this->params->get('yandexfotkiMaxPhotosPerRow') == 0)
    echo "</tr>";
}
echo "</table></div>";

echo "</p>";

?>
