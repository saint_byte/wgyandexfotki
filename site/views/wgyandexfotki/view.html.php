<?php

/**
 * 
 * Copyrights         : WG
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 */

jimport( 'joomla.application.component.view');

class wgyandexfotkiViewwgyandexfotki extends JView
{
        function display($tpl = null)
        {
                $albumsList = $this->get( 'AlbumsList' );
                $this->assignRef( 'albumsList',        $albumsList );
                // 2011/8/28/patch; later parameters
                $params = $this->get('Params');
                $this->assignRef( 'params', $params );
                parent::display($tpl);
        }

}

