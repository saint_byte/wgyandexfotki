<?php

/**
 * 
 * Copyrights         : WG
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

$mainframe = JFactory::getApplication();
$maindoc = JFactory::getDocument();

$jquery = true;
if ($jquery) {

        //$script = "<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js'></script>";
        //$script = "<script type='text/javascript' src='http://code.jquery.com/jquery-latest.min.js'></script>";
        $script = "<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js'></script>";
        $maindoc->addCustomTag($script);
        $maindoc->addCustomTag( '<script type="text/javascript">jQuery.noConflict();</script>' );
        $slimboxScript = "<script type='text/javascript' src='" . $this->baseurl . "/components/com_wgyandexfotki/libraries/slimbox/slimbox2.js'></script>";
        $maindoc->addCustomTag($slimboxScript);
        $slimboxCss = "<link rel='stylesheet' href='" . $this->baseurl . "/components/com_wgyandexfotki/libraries/slimbox/slimbox2.css' type='text/css' media='screen' />";
        $maindoc->addCustomTag($slimboxCss);
        
} else {
        $script = "<script type='text/javascript' src='" . $this->baseurl . "/media/system/js/mootools.js'></script>";
        $slimboxScript = "<script type='text/javascript' src='" . $this->baseurl . "/components/com_wgyandexfotki/libraries/slimbox/slimbox.js'></script>";
        $maindoc->addCustomTag($script);
        $maindoc->addCustomTag($slimboxScript);
        $slimboxCss = "<link rel='stylesheet' href='" . $this->baseurl . "/components/com_wgyandexfotki/libraries/slimbox/slimbox.css' type='text/css' media='screen' />";
        $maindoc->addCustomTag($slimboxCss);
}

$db = JFactory::getDBO();

$maxPhotosPerPage = $this->params->get('yandexfotkiMaxPhotosPerPage');
if ($maxPhotosPerPage < 1) $maxPhotosPerPage = 15;
$currentPage = $db->getEscaped(JRequest::getVar('page'));
$totPages = ceil ($this->numPhotos / $maxPhotosPerPage);
$pagnum = $this->params->get('yandexfotkiPagingNumbers');
if ($pagnum=='') $pagnum = 10;
$album = $db->getEscaped(JRequest::getVar('album'));

// if 0 photos then "empty"

echo "<h1>" . $this->albumTitle  . "</h1>";

if ($totPages > 0) {

        echo '<p>'.JText::_('WGP_album_contains') . " <strong>" . $this->numPhotos . "</strong> " . JText::_('WGP_photos');
        if ($totPages > 1) echo ' ('.$totPages.' '.JText::_('WGP_pages').').</p>';

        $paging = '';

        if ($totPages > 1) {

                $paging = '';
                if ($totPages > $pagnum) $paging .= '<a href="'.JRoute::_( 'index.php?option=com_wgyandexfotki&view=album&album=' . $album.'&page=1') . '">' . JText::_('WGP_first').'</a>'."&nbsp;&nbsp;&nbsp;";
                if ($currentPage > 1) $paging .= "<a href=\"".JRoute::_( 'index.php?option=com_wgyandexfotki&view=album&album='.$album.'&page='.($currentPage-1))."\">";
                $paging .= JText::_('WGP_prev');
                if ($currentPage > 1)$paging .= "</a>";
                $paging .= "&nbsp;&nbsp;";
                $midnum = ceil($pagnum / 2);
                $maxi = (($currentPage > $midnum) ? $currentPage + $midnum : $pagnum);
                $maxi = (($maxi > $totPages) ? $totPages : $maxi);
                $mini = (($currentPage > $midnum) ? ($currentPage - ($pagnum - ($maxi - $currentPage + 1))) : 1);
                $mini = (($mini < 1) ? 1 : $mini);
                //wglog::logLine("midnum=$midnum, i: $mini t/m $maxi");
                for ($i=$mini;$i<=$maxi;$i++) {
                        if ($i==$currentPage) { 
                                $paging .= '&nbsp;'.$i;
                        } else {
                                $paging .= '&nbsp;<a href="'.JRoute::_( 'index.php?option=com_wgyandexfotki&view=album&album='.$album.'&page='.$i)."\">$i</a>";
                        }
                        $paging .= "&nbsp;&nbsp;";
                }
                if ($currentPage < $totPages) $paging .= "&nbsp;<a href=\"" . JRoute::_( 'index.php?option=com_wgyandexfotki&view=album&album='.$album.'&page='.($currentPage+1)) . "\">";
                $paging .= JText::_('WGP_next');
                if ($currentPage < $totPages) $paging .= "</a>";
                if ($totPages > $pagnum) $paging .= "&nbsp;&nbsp;&nbsp;".'<a href="'.JRoute::_( 'index.php?option=com_wgyandexfotki&view=album&album='.$album. "&page=$totPages").'">'.JText::_('WGP_last').'</a>';

        }

        echo "<div class=\"wgyandexfotki\"><table align='center'>";
        if ($paging) echo "<tr><td align=\"right\" colspan=\"". $this->params->get('yandexfotkiMaxPhotosPerRow')."\">".$paging."</td></tr>";
        echo "<tr>";
        for ($i = 0; $i < sizeof($this->photosList); $i++){
                $photo = $this->photosList[$i];
                if ($i % $this->params->get('yandexfotkiMaxPhotosPerRow') == 0) echo "<tr valign=\"middle\" >";
                echo "<td align=\"center\">";
                echo "<a href='" . $photo['photoURL'] . "' ";
                echo "rel='lightbox[" . $db->getEscaped(JRequest::getVar('album')) . "]' ";
                echo ">";
                echo "<img ";
                echo "src='" . $photo['thumbURL'] . "' alt='". $photo['photoTitle'] . "' /></a>";
                echo "</td>";
                if ( ($i + $this->params->get('yandexfotkiMaxPhotosPerRow') + 1) % $this->params->get('yandexfotkiMaxPhotosPerRow') == 0) echo "</tr>";
        }
        if ($paging) echo "<tr><td align=\"right\" colspan=\"". $this->params->get('yandexfotkiMaxPhotosPerRow')."\">".$paging."</td></tr>";
        echo "</table></div>";

        //if ($paging) echo $paging;
        
        if (!$this->isSingle) {
                $backlist = JText::_('WGP_BACKTOLIST');
                if ($backlist) echo '<p>'.'<a href="'.JRoute::_( 'index.php?option=com_wgyandexfotki&view=wgyandexfotki').'">'.$backlist.'</a>'."</p>";
        }

} else {

        echo JText::_('WGP_nophotos') . '<br />';

}

?>
