<?php
/**
 * wgCache; cache variables
 * Use JSession object from Joomla with Database stroage to store (big) values in database session table
 *
 * Version		: 1.0.1
 * Copyrights 	: WG
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

class wgCache {
	
	// static
	static function saveVar($name,$value) {
		$session = JFactory::getSession('Database');
		$session->set($name,$value);		
	}
	
	// static
	static function loadVar($name) {
		$session = JFactory::getSession('Database');
		$value = $session->get($name);
		return $value;
	}
	
	// static
	static function hasVar($name) {
		$session = JFactory::getSession('Database');
		return $session->has($name);
	}
	
	// static
	static function clearVar($name) {
		$session = JFactory::getSession('Database');
		$value = $session->clear($name);
	}
	
}


?>