<?php 

/**
 * Helper class for logging/debugging
 *
 * Version		: 1.0.1
 * Copyrights 	: WG
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 */

class wgLog {

	// static
	function logLine($text,$htmlline='<br />') {
		echo date('H:i:s').'> '.$text.$htmlline;
	}
	
	// static
	function logDump($var,$htmlline='<br />') {
		
		$dump = print_r($var, true);
		$dump = str_replace("  ",'&nbsp;&nbsp;',$dump);
		$dumplines = split("\n",$dump);
		while (list($key,$line)=each($dumplines)) {
			echo date('H:i:s').'> '.$line.$htmlline;
		}
		
	}

}

?>