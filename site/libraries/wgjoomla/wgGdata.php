<?php
/**
 * WG gdata component; loading from google yandexfotki albums/photos
 * 
 * Version                : 1.0.1
 * Copyrights         : WG
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class wgGdata {

        static $_wgauth = array(
        'username' => '',
        'password' => '',
        'authmode' => 'public',
        'authid' => ''
        );
        static $_albumPhotos = array();

        // static
        static function init($username,$password='',$authmode='') {

                if ($username) self::$_wgauth['username'] = $username;
                if ($password) self::$_wgauth['password'] = $password;
                if ($authmode) self::$_wgauth['authmode'] = $authmode;
        }

        // static
        static function inityandexfotki($username,$password='',$authmode='',$thumbnailimgmax=160,$imgmax=400) {

                self::init($username,$password,$authmode);
                self::$_wgauth['thumbnailimgmax'] = ($thumbnailimgmax!='')?$thumbnailimgmax:'XXS';
                self::$_wgauth['imgmax'] = $imgmax;
        }

        // static
        static function resetyandexfotkiCache() {

                wgCache::saveVar('wgpAlbumPhotos','');
                wgCache::saveVar('wgpAlbumPhotosAlbumid','');
        }

        // static
        static function getyandexfotkiAlbums($start=0,$length=0) {

                // use no sessioncache -> if album change in yandexfotki, then directly seen
                $xmldata = '';
                if ($xmldata=='') {
                        $url = 'http://api-fotki.yandex.ru/api/users/'. self::$_wgauth['username'].'/albums/published/?limit=100&format=json';
                        //$url = 'http://api-fotki.yandex.ru/api/users/'. self::$_wgauth['username'].'/album/'..'/photos/rupdated?limit=100&format=json&oauth_token=&callback=Request.JSONP.request_map.request_3';
                        //"http://yandexfotkiweb.google.com/data/feed/api/user/" . self::$_wgauth['username']. '?thumbsize=' . self::$_wgauth['thumbnailimgmax'] . 'c'
                        $ch = curl_init($url);
                        //if (self::$_wgauth['authmode']!='public') {
                        //        $header[] = 'Authorization: GoogleLogin auth='.$_wgauth['authid'];
                        //        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                        //        curl_setopt($ch, CURLOPT_HEADER, false);
                        //}
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $xmldata = curl_exec($ch);
                        curl_close($ch);
                }
                $data = json_decode($xmldata,true);
                //print '<pre>'; print_r($data); print '</pre>'; die();
                $albumList = array();
                $numPhotos = 0;
                if(is_array($data) && is_array($data['entries'])) {
                        foreach ($data['entries'] as $entry) {
                                if (($numPhotos >= $start) && ($length==0 || ($numPhotos - $start)<$length)) {
                                        // collect for each album
                                        // - title
                                        // - url
                                        // - id
                                        // Gphoto namespace data
                                        // $ns_gphoto = $entry->children($namespace['gphoto']);
                                        // Media namespace data
                                        // $ns_media = $entry->children($namespace['media']);
                                        // Media thumbnail attributes
                                        // $thb_attr = $ns_media->group->thumbnail->attributes();
                                        // Media content attributes
                                        // $con_attr = $ns_media->group->content->attributes();
					     $imgs = @self::$_wgauth['thumbnailimgmax'];
                                        if ($imgs == '') $imgs = 'S';
					     $imgm = 	@self::$_wgauth['imgmax'];
					     if ($imgm == '') $imgm = 'XXS';
                                        $albumObject =         array();
                                        $albumObject['albumTitle'] = (string)$entry['title'];
                                        if (@$entry['img']){
                                        $albumObject['photoURL'] = (string) @$entry['img'][$imgm]['href'];
                                        $albumObject['thumbURL'] = (string) @$entry['img'][$imgs]['href'];
                                        } else {
                                        $albumObject['photoURL'] = '';
                                        $albumObject['thumbURL'] = '';
                                        }
                                        $id = substr($entry['id'],strrpos($entry['id'],':')+1);
                                        $albumObject['albumID'] = $id;
                                
                                        $albumList[] = $albumObject;
                                        $numPhotos += 1;
                                }
                        }
                        // always reset album cache for allowing refresh from dynamic yandexfotki
                        wgCache::saveVar('wgAlbumPhotosAlbumid','');
                }
                return $albumList;
        }
        static function getOnePage($url)
        {
            //if (!empty($start)) $start=';'.$start;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $data = curl_exec($ch);
            curl_close($ch);
            return json_decode($data,true);
        }
        static function getyandexfotkiAlbumphotos($albumid,$start=0,$length=0) {
                
                // check last cached and filled
                $incache = wgCache::loadVar('wgAlbumPhotosAlbumid');
                $albumcache = wgCache::loadVar('wgAlbumPhotos');
                //die('123');
                if (!is_array($albumcache) || ($incache!=$albumid)) {
                        //print self::$_wgauth['username'].','.$albumid;
                        $url = 'http://api-fotki.yandex.ru/api/users/'. self::$_wgauth['username'].'/album/'.$albumid.'/photos/rupdated/?limit=100&format=json&oauth_token=';
                        //$data = self::getOnePage($url);
                        //print $url; die();
                        $empties = array();
                        do {
                            $data = self::getOnePage($url);
                            //print '<pre>'; print_r($data); print '</pre>'; die();
                            foreach($data['entries'] as $item)
                            {
                                $empties[] = $item;
                            }
                            $url = @$data['links']['next'];
                        } while ($url != '');
                        //print '<pre>'; print_r($empties); print '</pre>'; die();
                        //$url = "http://yandexfotkiweb.google.com/data/feed/api/user/" . self::$_wgauth['username'] . '/albumid/' . $albumid . '?imgmax=' . self::$_wgauth['imgmax'] . '&thumbsize=' .self::$_wgauth['thumbnailimgmax'].'c';
                        //$ch = curl_init($url);
                        
                        //if (self::$_wgauth['authmode']!='public') {
                        //        $header[] = 'Authorization: GoogleLogin auth='.self::$_wgauth['authid'];
                        //        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                        //        curl_setopt($ch, CURLOPT_HEADER, false);
                        //}
                        //curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        //$xmldata = curl_exec($ch);
                        //curl_close($ch);
                        $numPhotos = 0; $albumTitle = ''; $photos = array();
                        if (count($empties)>0) {
                                //$data = simplexml_load_string($xmldata);
                                //$namespace = $data->getDocNamespaces();
                                //$albumTitle = (string)$data->title[0];
                                foreach ($empties as $entry) {
                                        // Collect for each photo;
                                        // - title
                                        // - url
                                        // - thumbnail url
                                        // Gphoto namespace data
                                        // Media content attributes
                                        //$con_attr = $s_media->group->content->attributes();
                                        // thumbnail is same reference, except other 's<imgmax>' tag
                                        //'thumbURL' => str_replace('s'.self::$_wgauth['imgmax'],'s'.self::$_wgauth['thumbnailimgmax'],$con_attr['url'])
                                        $photo = array();
                                        $photo['photoTitle'] = (string)$entry['title'];
                                        $photo['photoURL']   = (string)$entry['img'][self::$_wgauth['imgmax']]['href'];
                                        $photo['thumbURL']   = (string)$entry['img'][self::$_wgauth['thumbnailimgmax']]['href'];
                                        
                                        // 2011/5/26/Gs: PATCHED -> google returns NO thumbnail URL anymore!? -> make our own
                                        //$photo['thumbURL'] = str_replace('/d/','/s'. self::$_wgauth['thumbnailimgmax'].'-c/', $photo['thumbURL']);
                                        $photos[] = $photo;
                                        $numPhotos++;
                                }
                        }
                        //print '<pre>'; print_r($photos); print '</pre>'; die();
                        $albumcache = array(
                                'albumTitle' => $albumTitle,
                                'numPhotos'  => $numPhotos,
                                'photosList' => $photos
                        );
                        wgCache::saveVar('wgAlbumPhotosAlbumid',$albumid);
                        wgCache::saveVar('wgAlbumPhotos',$albumcache);
                }
                // load specific page
                $photos = $albumcache['photosList'];
                $numPhotos = $albumcache['numPhotos'];
                if ($length==0 || ($start+$length)>$numPhotos) $length = $numPhotos - $start;
                $photoList = array();
                for ($i=$start;$i<($start + $length);$i++) {
                        $photoList[] = $photos[$i];
                }
                $photoAlbum = array(
                        'albumTitle' => $albumcache['albumTitle'],
                        'numPhotos' => $numPhotos,
                        'photosList' => $photoList);
                return $photoAlbum;
        }

}

?>
