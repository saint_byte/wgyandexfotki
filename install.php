<?php

/**
 * 
 * Copyrights         : WG
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * 
 */

defined('_JEXEC') or die ('Restricted access');

define('WG_COM','com_wgyandexfotki');

class com_wgyandexfotkiInstallerScript
{
        /**
         * method to install the component
         *
         * @return void
         */
        function install($parent) {
                // $parent is the class calling this method
                //$parent->getParent()->setRedirectURL('index.php?option=com_helloworld');
                echo '<p>' . 'INSTALL' . '</p>';
                com_install();
        }

        /**
         * method to uninstall the component
         *
         * @return void
         */
        function uninstall($parent) {
                // $parent is the class calling this method
                echo '<p>' . 'UNINSTALL' . '</p>';
                com_uninstall();
        }

        /**
         * method to update the component
         *
         * @return void
         */
        function update($parent)
        {
                // $parent is the class calling this method
                echo '<p>' . 'UPDATE' . '</p>';
                com_install();
        }

        /**
         * method to run before an install/update/uninstall method
         *
         * @return void
         */
        function preflight($type, $parent)
        {
                // $parent is the class calling this method
                // $type is the type of change (install, update or discover_install)
        }

        /**
         * method to run after an install/update/uninstall method
         *
         * @return void
         */
        function postflight($type, $parent)
        {
                // $parent is the class calling this method
                // $type is the type of change (install, update or discover_install)
        }
}

/**
 * Installer function
 * @return
 */
function com_install() {
        
        $mainframe = JFactory::getApplication();
    $db = & JFactory::getDBO();

    jimport('joomla.filesystem.folder');
    jimport('joomla.filesystem.file');

    $path = JPATH_ADMINISTRATOR.DS.'components'.DS.WG_COM;

    jimport('joomla.installer.installer');
    $installer = & JInstaller::getInstance();

    $source         = $installer->getPath('source');

    $language = & JFactory::getLanguage();
    $language->load(WG_COM, JPATH_ROOT);
    
    $out = file_get_contents($path.DS.'readme.txt');

    $installer->set('extension.message', $out);    
    
}

/**
 * Uninstall function
 * @return
 */
function com_uninstall() {
        
        
}

?>
